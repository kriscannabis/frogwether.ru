

function clouds(cloud: number) {

  if (cloud > 70) return 3;
  else if (cloud > 30) return 2;
  else if (cloud > 10) return 1;
  else if (cloud < 10) return 0;
  
};

export default clouds;