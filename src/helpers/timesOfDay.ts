

function timesOfDay(time: number) {

  if (time === 9) return 'утро';
  else if (time === 15) return 'день';
  else if (time === 21) return 'вечер';
  else if (time === 3) return 'ночь';
  
};

export default timesOfDay;