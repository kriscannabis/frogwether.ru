
export enum dateWeather {
    "nowtime" = '/',
    "today" = '/today',
    "tomorrow" = '/tomorrow',
    "tendays" = '/tendays',
}