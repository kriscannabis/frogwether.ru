import React from "react";
import clouds from "src/helpers/clouds"
import wind from "src/helpers/wind"
import { Nowtime } from "src/types/weather/nowtime"

type Props = {
  weatherNow: Nowtime;
}

const PictureWeatherHok = ()=> {
    

  const PictureWeatherBase: React.FC<Props> = ({weatherNow})=> {
    
    return  <div className="weather_main__picture_weather">
      <span>{wind(weatherNow.wind.speed)}</span>
      <span>{clouds(weatherNow.clouds.all)} облаков</span>
      <span>{weatherNow.rain ? 'дождь' : 'нет дождя'}</span>
      <span style={{fontSize: '19px'}}>{weatherNow.weather[0].description}</span>
    </div>

  }


  return PictureWeatherBase
}

export default PictureWeatherHok