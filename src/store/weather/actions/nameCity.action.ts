import { createAsyncThunk } from "@reduxjs/toolkit";
import { createAPI, dataUser, KEY } from "../createApi";



const api = createAPI();

export const nameCityAction = createAsyncThunk(
    'weather/nameCity',
    async (_, thunkAPI) => {
        
        const secondPartURL = `/geo/1.0/reverse?lat=${dataUser.lat}&lon=${dataUser.lon}&appid=${KEY}`;

        try {
            const res = await api.get<any>(`${secondPartURL}`);
            return res.data;
        } catch (e) {
            console.log('error');
        };
        
    }
);