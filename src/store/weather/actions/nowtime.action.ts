import { createAsyncThunk } from "@reduxjs/toolkit";
import { createAPI, dataUser, KEY } from "../createApi";


const api = createAPI();


export const nowtimeAction = createAsyncThunk(
    'weather/nowtime',
    async (_, thunkAPI) => {

        
        let secondPartURL = `/data/2.5/weather?lat=${dataUser.lat}&lon=${dataUser.lon}&units=imperial&appid=${KEY}&lang=ru`;
        dataUser.main && (secondPartURL = `/data/2.5/weather?q=${dataUser.main.trim()}&appid=${KEY}&lang=ru`);

        try {
            const res = await api.get<any>(`${secondPartURL}`);
            return res.data;
        } catch (e) {
            console.log('error ', e);
        }
        
    }
);