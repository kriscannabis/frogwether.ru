import { createAsyncThunk } from "@reduxjs/toolkit";
import { createAPI, dataUser, KEY } from "../createApi";



const api = createAPI();


export const fiveDaysAction = createAsyncThunk(
  'weather/fiveDays',
  async (_, thunkAPI) => {

    
    let secondPartURL = `/data/2.5/forecast?lat=${dataUser.lat}&lon=${dataUser.lon}&appid=${KEY}&lang=ru`;
    dataUser.main && (secondPartURL = `/data/2.5/forecast?q=${dataUser.main.trim()}&appid=${KEY}&lang=ru`);
    

    try {
      const res = await api.get<any>(`${secondPartURL}`);
      return res.data;
    } catch (error) {
      console.log('error ', error);
    };
      
    
  }
);