import './nowtimeWeather.scss';
import { useAppSelector } from "../../../hooks/redux";
import Temperature from './Temperature';
import PictureWeather from './PictureWeather';
import converTemp from 'src/helpers/temperature';
import hours from 'src/helpers/hours';
import clouds from 'src/helpers/clouds';
import wind from 'src/helpers/wind';
import Now from './Now';
import Pop from './Pop';
import Wind from './Wind';


function NowtimeWeather(): JSX.Element {


  const selector = useAppSelector(state => state);
  const nowtimeWeather = selector.nowtimeWeatherReducer;
  const fiveDaysWeather = selector.fiveDaysWeatherReducer;


  const listFiveDays = fiveDaysWeather.main.list;


  return (
    <main className="main weather_main">

      <div className="weather_main__top">
        <Temperature 
          nowtimeWeather={nowtimeWeather.main}
          fiveDaysWeather={fiveDaysWeather.main}
        />

        <PictureWeather 
          weatherNow={nowtimeWeather.main}
        />
      </div>


      <span style={{display:"none"}}>видимость: 
        {nowtimeWeather.main.visibility > 1000 && nowtimeWeather.main.visibility / 1000} 
        {nowtimeWeather.main.visibility > 1000 ? 'км' : 'метров'}
      </span>

        
      <div className="day">
        {
          listFiveDays
          .filter((item, index) => (0 < index && index < 9))
          .map((item) => {
            const chour = hours(item.dt_txt);
            return <div key={item.dt} className="day__item">
              <div>

                {/* для солнца или луны */}
                <div>{item.sys.pod === 'd' ? 'день' : 'ночь'}</div>


                <div>{wind(item.wind.speed)}</div>
                <div>облака: {clouds(item.clouds.all)}</div>
                <div>дождь: {item.rain ? 'true' : 'false'}</div>
                <div style={{position: 'absolute', top: `${5* converTemp(item.main.temp, 'kelvin')}px`}}>{converTemp(item.main.temp, 'kelvin')}°</div>
                <div>{chour} : 00</div>
              </div>
            </div>
          })
        }
      </div>

      <span className="weather_main__name_station">
        Ближайшая метеостанци распологается в населенном пункте "{nowtimeWeather.main.name}"
      </span>

      <Now 
        humidity={nowtimeWeather.main.main.humidity}
        pressure={nowtimeWeather.main.main.pressure}
        visibility={nowtimeWeather.main.visibility}
      />

      <Pop list={listFiveDays}/>

      <Wind list={fiveDaysWeather.main.list}/>
    </main>
  );
};

export default NowtimeWeather;
