import hours from "src/helpers/hours";
import { ListFiveDays } from "src/types/weather/fiveDays";



type Rain = {
    list: ListFiveDays[]
}



function Pop({list}: Rain): JSX.Element {

    
    return <div>
        <h3>Осадки</h3>
        <div style={{display: 'flex', justifyContent: 'spece-between'}}>
        {
            list
            .filter((item, index) => (index < 6))
            .map((item)=> {
                return <div style={{display: 'flex', flexDirection: 'column'}} key={item.dt}>
                    <span>вероятность {item.pop}%</span>
                    <span>объем {!!list[0].rain ?  list[0].rain['3h'] : '0' }мм</span>
                    <span>время {hours(item.dt_txt)} : 00</span>
                </div>
            })
        }
        </div>
    </div>
}


export default Pop;