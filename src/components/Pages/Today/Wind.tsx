import hours from "src/helpers/hours";
import { ListFiveDays } from "src/types/weather/fiveDays";



type Props = {
    list: ListFiveDays[];
};


function Wind({list}: Props): JSX.Element {


    return <div>
        <h3>Ветер</h3>
        <div style={{display: 'flex', justifyContent: 'spece-between'}}>
            {
                list
                .filter((item, index)=> index < 6)
                .map((item) => <div style={{display: 'flex', flexDirection: 'column'}} key={item.dt}>
                    <span>
                        направление ветра
                        <div style={{transform: `rotate(${item.wind.deg}deg)`, fontSize: "20px", width: "20px", height: "20px", margin: "10px"}}>↑</div>
                    </span>
                    <span>скорость ветра {item.wind.speed}</span>
                    <span>{hours(item.dt_txt)} : 00</span>
                </div>)
            }
        </div>
    </div>
}


export default Wind;