import clouds from "src/helpers/clouds";
import wind from "src/helpers/wind";
//@ts-ignore
import stormIcon from './assets/storm.svg'
//@ts-ignore
import drizzleIcon from './assets/drizzle.svg'
//@ts-ignore
import rainIcon from './assets/rain.svg'
//@ts-ignore
import clearIcon from './assets/clear.svg'
//@ts-ignore
import cloudIcon from './assets/cloud.svg'

// import PictureWeatherHok from "src/hoks/pictureWeather"

type WeatherMain = {
    // humidity: number;
    // windSpeed: number;
    // clouds: number;
    weatherNow: any;
}

function PictureWeather(props: any) {
    const {weatherNow}:WeatherMain = props;
    const id = weatherNow.weather[0].id;

    const icons = {
      //гроза
      200:stormIcon,
      201:stormIcon,
      202:stormIcon,
      210:stormIcon,
      212:stormIcon,
      221:stormIcon,
      230:stormIcon,
      231:stormIcon,
      232:stormIcon,
      //морось
      300:drizzleIcon,
      301:drizzleIcon,
      302:drizzleIcon,
      310:drizzleIcon,
      311:drizzleIcon,
      312:drizzleIcon,
      313:drizzleIcon,
      314:drizzleIcon,
      321:drizzleIcon,
      //дождь
      500:rainIcon,
      501:rainIcon,
      502:rainIcon,
      503:rainIcon,
      504:rainIcon,
      511:rainIcon,
      520:rainIcon,
      521:rainIcon,
      522:rainIcon,
      531:rainIcon,
      //снег
      600:"",
      601:"",
      602:"",
      611:"",
      612:"",
      613:"",
      615:"",
      616:"",
      620:"",
      621:"",
      622:"",
      //атмосферные явления
      701:"",
      711:"",
      721:"",
      731:"",
      741:"",
      751:"",
      761:"",
      762:"",
      771:"",
      781:"",
      //чистое небо
      800:clearIcon,
      //облачность
      801:cloudIcon,
      802:cloudIcon,
      803:cloudIcon,
      804:cloudIcon,
    }
    
    //@ts-ignore
    return(<div><img src={icons[id]}/></div>)

    // return(
    //   <div className="weather_main__picture_weather">
    //       <span>{wind(weatherNow.wind.speed)}</span>
    //       <span>{clouds(weatherNow.clouds.all)} облаков</span>
    //       <span>{weatherNow.rain ? 'дождь' : 'нет дождя'}</span>
    //       <span style={{fontSize: '19px'}}>{weatherNow.weather[0].description}</span>
    //   </div>
    // )
}

export default PictureWeather

// const PictureWeather = PictureWeatherHok()

// export default PictureWeather