import hours from "src/helpers/hours";
import moreLess from "src/helpers/moreLess";
import converTemp from "src/helpers/temperature";
import timesOfDay from "src/helpers/timesOfDay";
//@ts-ignore
import s from './temperature.module.scss'

function Temperature(props: any) {
  const nowtimeWeather = props.nowtimeWeather;
  const fiveDaysWeather = props.fiveDaysWeather;
  const { feels_like, temp } = nowtimeWeather;
  const ifIt = !!feels_like || 0;

  const morningEvening = () => {
    let array = 0;

    return fiveDaysWeather.list
      .filter((item: any) => ((array <= 2) &&
        (hours(item.dt_txt) === 9 || hours(item.dt_txt) === 15 ||
          hours(item.dt_txt) === 21 || hours(item.dt_txt) === 15))
        && array++)
  }

  return (
    <div className={s.temperature}>
      <span>
        {
          morningEvening()?.map((item: any) => 
            <span key={item.dt}>
              {moreLess(converTemp(temp, 'fahrenheit'), converTemp(item.main.temp, 'kelvin'))}
              {converTemp(item.main.temp, 'kelvin')}°
              {timesOfDay(hours(item.dt_txt))}
            </span>
          )
        }
      </span>
      <div className={s.value}>
        {ifIt && converTemp(temp, 'fahrenheit')}°
      </div>
      <span>ощущается как: {ifIt && converTemp(feels_like, 'fahrenheit')}°</span>
    </div>
  )
}

export default Temperature;