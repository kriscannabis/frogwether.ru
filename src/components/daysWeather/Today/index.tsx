import { useAppSelector } from "../../../hooks/redux";
import converTemp from 'src/helpers/temperature';
// import PictureWeather from './components/PictureWeather';
// import Temperature from './components/Temperature/';
import clouds from 'src/helpers/clouds';
import hours from 'src/helpers/hours';
import wind from 'src/helpers/wind';
import './nowtimeWeather.scss';
//@ts-ignore
import s from './nowtime.module.scss'
// import Wind from './Wind';
// import Now from './components/Now';
// import Pop from './Pop';
import { Swiper, SwiperSlide } from "swiper/react";
// Import Swiper styles
import "swiper/css";
import "swiper/css/pagination";
import Temperature from "components/Pages/Today/components/Temperature";
import PictureWeather from "components/Pages/Today/components/PictureWeather";

function NowtimeWeather() {
  const selector = useAppSelector(state => state);
  const nowtimeWeather = selector.nowtimeWeatherReducer.main;
  const fiveDaysWeather = selector.fiveDaysWeatherReducer.main;

  return (
    <main className="main weather_main">
      <Swiper style={{ height: 'calc( 100vh - 120px )'}}
        direction={"vertical"}
        className="mySwiper"
      >
        <SwiperSlide style={{ height: '50vh' }}>
          <div className={s.oneSlide}>
            <Temperature
              nowtimeWeather={nowtimeWeather.main}
              fiveDaysWeather={fiveDaysWeather}
            />

            <PictureWeather
              weatherNow={nowtimeWeather}
              rain={fiveDaysWeather.list[1]}
            />
          </div>
            <div className={s.visibility}>видимость:
              {nowtimeWeather.visibility > 1000 && nowtimeWeather.visibility / 1000}
              {nowtimeWeather.visibility > 1000 ? 'км' : 'метров'}
            </div>
          
        </SwiperSlide>
        {/* <SwiperSlide>
          <div className="day">
            {
              fiveDaysWeather.list
                .filter((item, index) => (0 < index && index < 9))
                .map((item: any, index: any) => {
                  const chour = hours(item.dt_txt);
                  return <div key={item.dt} className="day__item">
                    <div>

                      
                      <div>{item.sys.pod === 'd' ? 'день' : 'ночь'}</div>


                      <div>{wind(item.wind.speed)}</div>
                      <div>облака: {clouds(item.clouds.all)}</div>
                      <div>дождь: {item.rain ? 'true' : 'false'}</div>
                      <div style={{ position: 'absolute', top: `${5 * converTemp(item.main.temp, 'kelvin')}px` }}>{converTemp(item.main.temp, 'kelvin')}°</div>
                      <div>{chour} : 00</div>
                    </div>
                  </div>
                })
            }
          </div>
        </SwiperSlide> */}
        {/* <SwiperSlide>
          <span className="weather_main__name_station">
            Ближайшая метеостанци распологается в населенном пункте "{nowtimeWeather.name}"
          </span>

          <Now
            humidity={nowtimeWeather.main.humidity}
            pressure={nowtimeWeather.main.pressure}
            visibility={nowtimeWeather.visibility}
          />
        </SwiperSlide> */}
        {/* <SwiperSlide>
          <Pop list={fiveDaysWeather.list} />
          <Wind list={fiveDaysWeather.list} />
        </SwiperSlide> */}

      </Swiper>



    </main>
  );
};

export default NowtimeWeather;
