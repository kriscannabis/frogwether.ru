import { Route, Routes } from "react-router-dom";
import { dateWeather } from "../routes";
//@ts-ignore
import s from './main.module.scss';
import TenWeather from "./Pages/FiveDays"
import TomorrowWeather from "./Pages/Tomorrow";
import NowtimeWeather from "./Pages/Today";

function Main(): JSX.Element {

  return (
    <main className={s.main}>
      <Routes>
        <Route path={`${dateWeather.nowtime}`} element={ <NowtimeWeather /> } />
        <Route path={`${dateWeather.tomorrow}`} element={ <TomorrowWeather /> } />
        <Route path={`${dateWeather.tendays}`} element={ <TenWeather /> } />
      </Routes>
    </main>
  );
}

export default Main;
