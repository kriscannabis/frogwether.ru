import { MouseEvent, useRef, useState } from "react";
import { useAppDispatch } from "../../hooks/redux";
import s from './searchcity.module.scss'
import glassIcon from './assets/glass.svg'
import { dataUser } from "src/store/weather/createApi";
import useTimeout from "src/helpers/useTimeout";
import { nowtimeAction } from "src/store/weather/actions/nowtime.action";
import { fiveDaysAction } from "src/store/weather/actions/fiveDays.action";


type DataUser = {
  lat: number;
  lon: number;
  main: string;
}


function SearchCity(): JSX.Element {


  const dispatch = useAppDispatch();


  const [country, setCountry] = useState<number>(0);
  const [isDataUser, setDataUser] = useState<DataUser>(dataUser);


  const inputEl = useRef<any>(null);
  const searchCity = useRef<any>(null);


  const listCitiesHandler = (e: MouseEvent<HTMLUListElement>)=> {
    const target = e.target as HTMLDListElement;
    dataUser.main = target.textContent as string;
    dispatch(nowtimeAction());
    dispatch(fiveDaysAction());
    setCountry(country+1);

    const indexCity = citiesRu.indexOf(inputEl.current.value)
    setDataUser({...isDataUser, main: citysEN[indexCity]});
    inputEl.current.value = '';
  };

  dataUser.main = isDataUser.main

  const citysEN = [
    "pankovka",
    "abakan",
    "almetyevsk",
    "anadyr",
    "anapa",
    "arkhangelsk",
    "astrakhan",
    "barnaul",
    "belgorod",
    "beslan",
    "biysk",
    "birobidzhan",
    "blagoveshchensk",
    "bologoye",
    "bryansk",
    "veliky novgorod",
    "veliky ustyug",
    "vladivostok",
    "vladikavkaz",
    "vladimir",
    "volgograd",
    "vologda",
    "vorkuta",
    "voronezh",
    "gatchina",
    "gdov",
    "gelendzhik",
    "gorno-altaysk",
    "grozny",
    "gudermes",
    "gus-khrustalny",
    "dzerzhinsk",
    "dmitrov",
    "dubna",
    "yeysk",
    "yekaterinburg",
    "yelabuga",
    "yelets",
    "yessentuki",
    "zlatoust",
    "ivanovo",
    "izhevsk",
    "irkutsk",
    "yoshkar-ola",
    "kazan",
    "kaliningrad",
    "kaluga",
    "kemerovo",
    "kislovodsk",
    "komsomolsk-on-amur",
    "kotlas",
    "krasnodar",
    "krasnoyarsk",
    "kurgan",
    "kursk",
    "kyzyl",
    "leninogorsk",
    "lensk",
    "lipetsk",
    "luga",
    "lyuban",
    "lyubertsy",
    "magadan",
    "maykop",
    "makhachkala",
    "miass",
    "mineralnye vody",
    "mirny",
    "moscow",
    "murmansk",
    "murom",
    "mytishchi",
    "naberezhnye chelny",
    "nadym",
    "nalchik",
    "nazran",
    "naryan-mar",
    "nakhodka",
    "nizhnevartovsk",
    "nizhnekamsk",
    "nizhny novgorod",
    "nizhny tagil",
    "novokuznetsk",
    "novosibirsk",
    "novy urengoy",
    "norilsk",
    "obninsk",
    "oktyabrsky",
    "omsk",
    "orenburg",
    "orekhovo-zuyevo",
    "oryol",
    "penza",
    "perm",
    "petrozavodsk",
    "petropavlovsk-kamchatsky",
    "podolsk",
    "pskov",
    "pyatigorsk",
    "rostov-on-don",
    "rybinsk",
    "ryazan",
    "salekhard",
    "samara",
    "saint petersburg",
    "saransk",
    "saratov",
    "severodvinsk",
    "smolensk",
    "sol-iletsk",
    "sochi",
    "stavropol",
    "surgut",
    "syktyvkar",
    "tambov",
    "tver",
    "tobolsk",
    "tolyatti",
    "tomsk",
    "tuapse",
    "tula",
    "tynda",
    "tyumen",
    "ulan-ude",
    "ulyanovsk",
    "ufa",
    "khabarovsk",
    "khanty-mansiysk",
    "chebarkul",
    "cheboksary",
    "chelyabinsk",
    "cherepovets",
    "cherkessk",
    "chistopol",
    "chita",
    "shadrinsk",
    "shatura",
    "shuya",
    "elista",
    "engels",
    "yuzhno-sakhalinsk",
    "yakutsk",
    "yaroslavl"
]

  const citiesRu = [
    "панковка",
    "абакан",
    "альметьевск",
    "анадырь",
    "анапа",
    "архангельск",
    "астрахань",
    "барнаул",
    "белгород",
    "беслан",
    "бийск",
    "биробиджан",
    "благовещенск",
    "бологое",
    "брянск",
    "великий новгород",
    "великий устюг",
    "владивосток",
    "владикавказ",
    "владимир",
    "волгоград",
    "вологда",
    "воркута",
    "воронеж",
    "гатчина",
    "гдов",
    "геленджик",
    "горно-алтайск",
    "грозный",
    "гудермес",
    "гусь-хрустальный",
    "дзержинск",
    "дмитров",
    "дубна",
    "ейск",
    "екатеринбург",
    "елабуга",
    "елец",
    "ессентуки",
    "златоуст",
    "иваново",
    "ижевск",
    "иркутск",
    "йошкар-ола",
    "казань",
    "калининград",
    "калуга",
    "кемерово",
    "кисловодск",
    "комсомольск-на-амуре",
    "котлас",
    "краснодар",
    "красноярск",
    "курган",
    "курск",
    "кызыл",
    "лениногорск",
    "ленск",
    "липецк",
    "луга",
    "любань",
    "люберцы",
    "магадан",
    "майкоп",
    "махачкала",
    "миасс",
    "минеральные воды",
    "мирный",
    "москва",
    "мурманск",
    "муром",
    "мытищи",
    "набережные челны",
    "надым",
    "нальчик",
    "назрань",
    "нарьян-мар",
    "находка",
    "нижневартовск",
    "нижнекамск",
    "нижний новгород",
    "нижний тагил",
    "новокузнецк",
    "новосибирск",
    "новый уренгой",
    "норильск",
    "обнинск",
    "октябрьский",
    "омск",
    "оренбург",
    "орехово-зуево",
    "орёл",
    "пенза",
    "пермь",
    "петрозаводск",
    "петропавловск-камчатский",
    "подольск",
    "псков",
    "пятигорск",
    "ростов-на-дону",
    "рыбинск",
    "рязань",
    "салехард",
    "самара",
    "санкт-петербург",
    "саранск",
    "саратов",
    "северодвинск",
    "смоленск",
    "соль-илецк",
    "сочи",
    "ставрополь",
    "сургут",
    "сыктывкар",
    "тамбов",
    "тверь",
    "тобольск",
    "тольятти",
    "томск",
    "туапсе",
    "тула",
    "тында",
    "тюмень",
    "улан-уде",
    "ульяновск",
    "уфа",
    "хабаровск",
    "ханты-мансийск",
    "чебаркуль",
    "чебоксары",
    "челябинск",
    "череповец",
    "черкесск",
    "чистополь",
    "чита",
    "шадринск",
    "шатура",
    "шуя",
    "элиста",
    "энгельс",
    "южно-сахалинск",
    "якутск",
    "ярославль"
]

  // const citys = ['moscow', 'pankovka', 'saint petersburg', 'novosibirsk', 'novgorod', 'ekaterinburg', 'kazan', ];
  const searchCitys = () => {
    
    const lengthWord = inputEl.current.value.length;

    return citiesRu.filter((item)=> {
      let newArray: string[] | string = [...item.split('')];
      newArray.length = lengthWord;
      newArray = newArray.join('');
      return newArray === inputEl.current.value;
      
    })
  };

  const timeout = useTimeout(200);
  const refreshPage = () => setCountry(country+1);
  const listCitysHandler = () => timeout(refreshPage);

  // useEffect(()=>{
  //   axios('http://frogwether.ru:4000/citys?city=%D0%BF%D0%B0%D0%BD',{
  //     method:'GET'
  //   })
  //   .then(r=>console.log(r))
  // },[])

  return (
    <div 
      className={s.searchCity}
      ref={searchCity} 
    >
      <input 
        placeholder="введите город"
        onChange={listCitysHandler}
        ref={inputEl} 
        type="text" 
        className={s.input} 
        style={{
          background: `url(${glassIcon}) no-repeat #ffffff`,
          backgroundPosition: "15px 15px"
        }}
      />


      {/* <PlaceSearch 
        listCitysHandler={listCitysHandler}
      /> */}


      <ul 
        className={s.listCitys}
        onClick={(e)=> listCitiesHandler(e)}
        // style={{top: `${searchCity.current.offsetHeight || 0}px`}}
      >
        {
          inputEl.current && inputEl.current.value && 
          searchCitys().map((item, index) => <li 
            style={{cursor: "pointer"}}
            key={index}
          > {item} </li>)
        }
      </ul>
      {/* <button onClick={onButtonClick}>button</button> */}
    </div>
  )
}



export default SearchCity